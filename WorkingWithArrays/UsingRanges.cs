﻿using System;

#pragma warning disable CA1062

namespace WorkingWithArrays
{
    public static class UsingRanges
    {
        public static int[] GetArrayWithAllElements(int[] array)
        {
            int[] newArray = array[..];
            return newArray;
        }

        public static int[] GetArrayWithoutFirstElement(int[] array)
        {
            int[] newArray = array[1..];
            return newArray;
        }

        public static int[] GetArrayWithoutTwoFirstElements(int[] array)
        {
            int[] newArray = array[2..];
            return newArray;
        }

        public static int[] GetArrayWithoutThreeFirstElements(int[] array)
        {
            int[] newArray = array[3..];
            return newArray;
        }

        public static int[] GetArrayWithoutLastElement(int[] array)
        {
            int[] newArray = array[..^1];
            return newArray;
        }

        public static int[] GetArrayWithoutTwoLastElements(int[] array)
        {
            int[] newArray = array[..^2];
            return newArray;
        }

        public static int[] GetArrayWithoutThreeLastElements(int[] array)
        {
            int[] newArray = array[..^3];
            return newArray;
        }

        public static bool[] GetArrayWithoutFirstAndLastElements(bool[] array)
        {
            bool[] newArray = array[1..^1];
            return newArray;
        }

        public static bool[] GetArrayWithoutTwoFirstAndTwoLastElements(bool[] array)
        {
            bool[] newArray = array[2..^2];
            return newArray;
        }

        public static bool[] GetArrayWithoutThreeFirstAndThreeLastElements(bool[] array)
        {
            bool[] newArray = array[3..^3];
            return newArray;
        }
    }
}
